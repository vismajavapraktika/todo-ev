package com.visma.todo.toolsForDurationType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CheckIfDurationIsCorrect {

	private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
	private static Calendar parsedDate;
	private static Integer[] tokensInt;

	public static boolean checkIfDurationIsCorrect(String type, String date) {

		tokensInt = new Integer[3];
		parsedDate = Calendar.getInstance();
		try {
			parsedDate.setTime(formatter.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		tokensInt[0] = parsedDate.get(Calendar.HOUR_OF_DAY);
		tokensInt[1] = parsedDate.get(Calendar.MINUTE);
		tokensInt[2] = parsedDate.get(Calendar.SECOND);
		if (!type.equals("Reminder")) {
			if ((tokensInt[2] > 0 && tokensInt[2] < 24 && tokensInt[1] >= 0 && tokensInt[1] < 60 && tokensInt[0] >= 0
					&& tokensInt[0] < 60)
					|| (tokensInt[2] >= 0 && tokensInt[2] < 24 && tokensInt[1] > 0 && tokensInt[1] < 60
							&& tokensInt[0] >= 0 && tokensInt[0] < 60)
					|| (tokensInt[2] >= 0 && tokensInt[2] < 24 && tokensInt[1] >= 0 && tokensInt[1] < 60
							&& tokensInt[0] > 0 && tokensInt[0] < 60)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (tokensInt[2] >= 0 && tokensInt[2] < 24 && tokensInt[1] >= 0 && tokensInt[1] < 60 && tokensInt[0] >= 0
					&& tokensInt[0] < 60) {
				return true;
			} else {
				return false;
			}
		}
	}

}
