package com.visma.todo.toolsForDurationType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZoneId;
import java.util.TimeZone;

@Component
public class ConvertDurationField {
	private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
	static {
		formatter.setLenient(false);
		formatter.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
	}

	public  Duration convertStringToDuration(String date) {
		try {
			return Duration.ofMillis(formatter.parse(date).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public String convertDurtionToString(Duration duration) {
		return duration.toHours() + ":" + duration.toMinutes() % 60 + ":"
				+ duration.getSeconds() % 60;
	}


}
