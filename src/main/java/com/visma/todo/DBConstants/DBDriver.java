package com.visma.todo.DBConstants;

import static com.visma.todo.DBConstants.DBConstants.DB_DRIVER;

public class DBDriver {

	public void DatabasePersistence() {
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
