package com.visma.todo.CollectData;

import com.visma.todo.input.UserInput;
import com.visma.todo.input.UserKeyboardInput;
import com.visma.todo.objectBodies.Meeting;
import com.visma.todo.objectBodies.Reminder;
import com.visma.todo.objectBodies.ReserveTime;
import com.visma.todo.toolsForDateType.ParseDate;
import com.visma.todo.toolsForDurationType.ConvertDurationField;
import org.springframework.stereotype.Component;

@Component
public class CollectDataForSQL {
	private static UserInput inputScanner = new UserKeyboardInput();
	private static Reminder reminder;
	private static Meeting meeting;
	private static ReserveTime reserveTime;
	private static ParseDate parser = new ParseDate();
	private static ConvertDurationField parser2 = new ConvertDurationField();

	public static void createObject(String Type) {
		if (Type.equals("Reminder")) {
			Reminder reminder = new Reminder();
			reminder.setType(Type);
			System.out.println("Enter tasks name: ");
			reminder.setName(inputScanner.getInput());
			System.out.println("Enter the task: ");
			reminder.setActions(inputScanner.getInput());
			System.out.println("Enter tasks location: ");
			reminder.setPlace(inputScanner.getInput());
			System.out.println("Enter tasks date(Year:month:day:hour:minute:second) : ");
			reminder.setDate(inputScanner.getInput());
			System.out.println("Enter tasks date(hour:minute:second) : ");
			reminder.setDuration(parser2.convertStringToDuration(inputScanner.getInput()));
			setReminder(reminder);
		} else if (Type.equals("Meeting")) {
			Meeting meeting = new Meeting();
			meeting.setType(Type);
			System.out.println("Enter tasks name: ");
			meeting.setName(inputScanner.getInput());
			System.out.println("Enter the task: ");
			meeting.setActions(inputScanner.getInput());
			System.out.println("Enter tasks location: ");
			meeting.setPlace(inputScanner.getInput());
			System.out.println("Enter tasks date(Year:month:day:hour:minute:second) : ");
			meeting.setDate(inputScanner.getInput());
			System.out.println("Enter tasks date(hour:minute:second) : ");
			parser2.convertStringToDuration(inputScanner.getInput());
			meeting.setDuration(parser2.convertStringToDuration(inputScanner.getInput()));
			System.out.println("Enter tasks Attendess: ");
			meeting.setListOfAttendess(inputScanner.getInput());
			setMeeting(meeting);
		} else {
			ReserveTime reserveTime = new ReserveTime();
			reserveTime.setType(Type);
			System.out.println("Enter tasks name: ");
			reserveTime.setName(inputScanner.getInput());
			System.out.println("Enter the task: ");
			reserveTime.setActions(inputScanner.getInput());
			System.out.println("Enter tasks location: ");
			reserveTime.setPlace(inputScanner.getInput());
			System.out.println("Enter tasks date(Year:month:day:hour:minute:second) : ");
			reserveTime.setDate(inputScanner.getInput());
			System.out.println("Enter tasks date(hour:minute:second) : ");
			parser2.convertStringToDuration(inputScanner.getInput());
			reserveTime.setDuration(parser2.convertStringToDuration(inputScanner.getInput()));
			setReserveTime(reserveTime);
		}

	}

	public static Reminder getReminder() {
		return reminder;
	}

	public static void setReminder(Reminder reminder) {
		CollectDataForSQL.reminder = reminder;
	}

	public static Meeting getMeeting() {
		return meeting;
	}

	public static void setMeeting(Meeting meeting) {
		CollectDataForSQL.meeting = meeting;
	}

	public static ReserveTime getReserveTime() {
		return reserveTime;
	}

	public static void setReserveTime(ReserveTime reserveTime) {
		CollectDataForSQL.reserveTime = reserveTime;
	}

}
