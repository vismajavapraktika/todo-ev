package com.visma.todo.CollectData;

import static com.visma.todo.DBConstants.DBConstants.DB_DRIVER;
import static com.visma.todo.DBConstants.DBConstants.DB_PASSWORD;
import static com.visma.todo.DBConstants.DBConstants.DB_URL;
import static com.visma.todo.DBConstants.DBConstants.DB_USERNAME;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.visma.todo.toolsForDateType.ParseDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.objectBodies.Meeting;
import com.visma.todo.objectBodies.Reminder;
import com.visma.todo.objectBodies.ReserveTime;

@Component
public class CollectDataFromSQL {
	@Autowired
	private ParseDate parse;

	private static final String FIND_ALL = "SELECT * from \"To_Do_Task\"";
	private static final String FIND_ALL_REMINDERS = "SELECT * from \"To_Do_Task\" WHERE \"Type\" = 'Reminder'";
	private static final String FIND_ALL_MEETINGS = "SELECT * from \"To_Do_Task\" WHERE \"Type\" = 'Meeting'";
	private static final String FIND_ALL_RESERVETIMES = "SELECT * from \"To_Do_Task\" WHERE \"Type\" = 'ReserveTime'";

	public CollectDataFromSQL() {
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Meeting> getWholeList() {
		List<Meeting> wholeList = new ArrayList<Meeting>();
		PreparedStatement statement = null;
		try (Connection conn = getConnection()) {
			statement = conn.prepareStatement(FIND_ALL);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer ID = resultSet.getInt("ID");
				String Type = resultSet.getString("Type");
				String Name = resultSet.getString("Name");
				String Actions = resultSet.getString("Actions");
				String Date = parse.convertDateToString(resultSet.getTimestamp("Date"));
				Duration Durations = Duration.ofSeconds(resultSet.getLong("Duration"));
				String Place = resultSet.getString("Place");
				String Attendess = resultSet.getString("Attendess");
				wholeList.add(new Meeting(ID, Type, Name, Actions, Date, Durations, Place, Attendess));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeStatement(statement);
		}
		return wholeList;
	}

	public List<Reminder> getReminderList() {
		List<Reminder> reminderList = new ArrayList<Reminder>();
		PreparedStatement statement = null;
		try (Connection conn = getConnection()) {
			statement = conn.prepareStatement(FIND_ALL_REMINDERS);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer ID = resultSet.getInt("ID");
				String Type = resultSet.getString("Type");
				String Name = resultSet.getString("Name");
				String Actions = resultSet.getString("Actions");
				String Date = parse.convertDateToString(resultSet.getTimestamp("Date"));
				Duration Durations = Duration.ofSeconds(resultSet.getLong("Duration"));
				String Place = resultSet.getString("Place");
				reminderList.add(new Reminder(ID, Type, Name, Actions, Date, Durations, Place));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeStatement(statement);
		}
		return reminderList;
	}


	public List<Meeting> getMeetingList() {
		List<Meeting> meetingList = new ArrayList<Meeting>();
		PreparedStatement statement = null;
		try (Connection conn = getConnection()) {
			statement = conn.prepareStatement(FIND_ALL_MEETINGS);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer ID = resultSet.getInt("ID");
				String Type = resultSet.getString("Type");
				String Name = resultSet.getString("Name");
				String Actions = resultSet.getString("Actions");
				String Date = parse.convertDateToString(resultSet.getTimestamp("Date"));
				Duration Durations = Duration.ofSeconds(resultSet.getLong("Duration"));
				String Place = resultSet.getString("Place");
				String Attendess = resultSet.getString("Attendess");
				meetingList.add(new Meeting(ID, Type, Name, Actions, Date, Durations, Place, Attendess));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeStatement(statement);
		}
		return meetingList;
	}

	public List<ReserveTime> getReserveTimeList() {
		List<ReserveTime> reserveTimeList = new ArrayList<ReserveTime>();
		PreparedStatement statement = null;
		try (Connection conn = getConnection()) {
			statement = conn.prepareStatement(FIND_ALL_RESERVETIMES);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Integer ID = resultSet.getInt("ID");
				String Type = resultSet.getString("Type");
				String Name = resultSet.getString("Name");
				String Actions = resultSet.getString("Actions");
				String Date = parse.convertDateToString(resultSet.getTimestamp("Date"));
				Duration Durations = Duration.ofSeconds(resultSet.getLong("Duration"));
				String Place = resultSet.getString("Place");
				reserveTimeList.add(new ReserveTime(ID, Type, Name, Actions, Date, Durations, Place));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeStatement(statement);
		}
		return reserveTimeList;
	}


	private static Connection getConnection() {
		try {
			return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private static void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

}
