package com.visma.todo.converters;

import com.visma.todo.toolsForDurationType.ConvertDurationField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * Created by edmundas.virsila on 8/4/2016.
 */
@Component
public class DurationToStringConverter implements Converter<Duration, String> {
    @Autowired
    private ConvertDurationField convertDurationField;
    @Override
    public String convert(Duration duration) {
        return convertDurationField.convertDurtionToString(duration);
    }
}
