package com.visma.todo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * Created by edmundas.virsila on 8/5/2016.
 */
@Component
public class DurationFromStringConverter implements Converter<String, Duration> {
    @Override
    public Duration convert(String s) {
        if (s == null) {
            return null;
        }

        return Duration.ofSeconds(Integer.valueOf(s));
    }
}
