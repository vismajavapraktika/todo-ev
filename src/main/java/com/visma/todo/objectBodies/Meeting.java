package com.visma.todo.objectBodies;

import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Date;
@Component
public class Meeting extends ToDoTask {

	String listOfAttendess;

	public Meeting() {
	};

	public Meeting(Integer ID, String type, String name, String actions, String date, Duration duration, String place,
			String listOfAttendess) {
		super(ID, type, name, actions, date, duration, place);
		this.listOfAttendess = listOfAttendess;
	}

	public String getListOfAttendess() {
		return listOfAttendess;
	}

	public void setListOfAttendess(String listOfAttendess) {
		this.listOfAttendess = listOfAttendess;
	}

//	@Override
//	public String toString() {
//		return "" + data + ", listOfAttendess=" + listOfAttendess + "\n";
//	}
}
