package com.visma.todo.objectBodies;

import java.time.Duration;
import java.util.Date;

public interface ToDoTaskInterface {

	Integer getId();

	void setId(Integer Id);

	String getType();

	void setType(String type);

	String getName();

	void setName(String name);

	String getActions();

	void setActions(String actions);

	String getDate();

	void setDate(String date);

	Duration getDuration();

	void setDuration(Duration duration);

	String getPlace();

	void setPlace(String place);

	String toString();

}