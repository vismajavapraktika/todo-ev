package com.visma.todo.objectBodies;

import java.time.Duration;
import java.util.Date;

public class ToDoTask implements ToDoTaskInterface {

	public Integer Id;
	public String type;
	public String date;
	public Duration duration;
	public String name;
	public String actions;
	public String place;

	public ToDoTask() {
	};

	public ToDoTask(Integer Id, String type, String name, String actions, String date, Duration duration, String place) {
		this.Id = Id;
		this.type = type;
		this.name = name;
		this.actions = actions;
		this.date = date;
		this.duration = duration;
		this.place = place;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getActions() {
		return actions;
	}

	@Override
	public void setActions(String actions) {
		this.actions = actions;
	}

	@Override
	public String getDate() {
		return date;
	}

	@Override
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public Duration getDuration() {
		return duration;
	}

	@Override
	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	@Override
	public String getPlace() {
		return place;
	}

	@Override
	public void setPlace(String place) {
		this.place = place;
	}

	@Override
	public Integer getId() {
		return Id;
	}

	@Override
	public void setId(Integer id) {
		this.Id = id;

	}

	public String toString() {
		return "ID=" + Id + ", type=" + type + ", name=" + name + ", actions=" + actions + ", date=" + date
				+ ", duration=" + duration.toHours() + ":" + duration.toMinutes() % 60 + ":"
				+ duration.getSeconds() % 60 + ", place=" + place;
	}

}