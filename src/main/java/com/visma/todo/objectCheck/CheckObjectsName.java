package com.visma.todo.objectCheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.CollectData.CollectDataFromSQL;
import com.visma.todo.objectBodies.Meeting;

@Component
public class CheckObjectsName {

	@Autowired
	private CollectDataFromSQL collectDataFromSQL;

	public boolean checkIfNameDoesntExist(String name) {

		for (Meeting ListCheck : collectDataFromSQL.getWholeList()) {
			if (name.equals(ListCheck.getName())) {
				return false;
			} else {
				return true;
			}
		}
		return true;
	}
}
