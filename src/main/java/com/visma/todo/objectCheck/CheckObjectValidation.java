package com.visma.todo.objectCheck;

import com.visma.todo.toolsForDateType.ParseDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.objectBodies.Meeting;
import com.visma.todo.objectBodies.Reminder;
import com.visma.todo.objectBodies.ReserveTime;
import com.visma.todo.toolsForDateType.CheckIfDateIsntOld;
import com.visma.todo.toolsForDateType.CheckIfTimeDoesntCollideWithOtherTime;

@Component
public class CheckObjectValidation {
	
	@Autowired
	private CheckObjectsName checkObjectsName;
	@Autowired
	private CheckIfTimeDoesntCollideWithOtherTime checkIfTimeDoesntCollideWithOtherTime;
	@Autowired
	private ParseDate parse;

	public boolean isReminderCorrect(Reminder reminder) {

		if (reminder.getName().equals(null) || !checkObjectsName.checkIfNameDoesntExist(reminder.getName())) {
			return false;
		} else {
			if (reminder.getDate().equals(null)) {
				return true;
			} else if (CheckIfDateIsntOld.checkIfNotOld(parse.convertStringToDate(reminder.getDate())) && checkIfTimeDoesntCollideWithOtherTime
					.checkingEnteredTimWithOtherTimes(reminder.getType(), parse.convertStringToDate(reminder.getDate()), reminder.getDuration())) {
				return true;
			}
			return false;
		}
	}

	public boolean isMeetingCorrect(Meeting meeting) {
		if (meeting.getName().equals(null) || meeting.getDate().equals(null)
				|| meeting.getDuration().getSeconds() == 0
				|| !checkObjectsName.checkIfNameDoesntExist(meeting.getName())) {
			return false;
		} else if (CheckIfDateIsntOld.checkIfNotOld(parse.convertStringToDate(meeting.getDate())) && checkIfTimeDoesntCollideWithOtherTime
				.checkingEnteredTimWithOtherTimes(meeting.getType(), parse.convertStringToDate(meeting.getDate()), meeting.getDuration())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isReserveTimeCorrect(ReserveTime reserveTime) {
		if (reserveTime.getName().equals(null) || reserveTime.getDate().equals(null)
				|| reserveTime.getDuration().getSeconds() == 0
				|| !checkObjectsName.checkIfNameDoesntExist(reserveTime.getName())) {
			return false;
		} else if (CheckIfDateIsntOld.checkIfNotOld(parse.convertStringToDate(reserveTime.getDate()))
				|| checkIfTimeDoesntCollideWithOtherTime.checkingEnteredTimWithOtherTimes(reserveTime.getType(),
				parse.convertStringToDate(reserveTime.getDate()), reserveTime.getDuration())) {
			return true;
		} else {
			return false;
		}
	}

}
