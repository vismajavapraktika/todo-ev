package com.visma.todo.mainMenuCases;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.visma.todo.input.UserInput;
import com.visma.todo.printPlainText.PrintText;
import com.visma.todo.repositoryEdit.EditTask;
import com.visma.todo.repositoryRead.PrintInfoFromDB;
import com.visma.todo.repositoryRemove.RemoveLineFromFile;
import com.visma.todo.startMenu.StartMenu;

@Component
public class MainMenuCases {
	@Autowired
	private UserInput inputScanner;
	@Autowired
	private AddToCases inputHandler;
	@Autowired
	private PrintFromDBCases inputHandler2;
	@Autowired
	@Qualifier("textWritingToFile")
	private PrintText printText;
	@Autowired
	private PrintInfoFromDB printInfoFromDB;
	@Autowired
	@Qualifier("textChangeTaskData")
	private PrintText printText2;
	@Autowired
	private EditTask editTask;
	@Autowired
	private RemoveLineFromFile removeLineFromFile;

	public void handleMainMenuInput(String input, StartMenu startMenu) throws IOException {
		switch (input) {
		case "1":
			System.out.println(printText.getText("Writing to file menu choices"));
			System.out.print(printText.getText("menu.input"));
			String inputInsert = inputScanner.getInput();
			inputHandler.handleAddToDBInput(inputInsert, this);
			break;
		case "2":
			System.out.println(printInfoFromDB.getText("Info From DB"));
			System.out.print(printText.getText("menu.input"));
			String inputPrint = inputScanner.getInput();
			inputHandler2.handlePrintInput(inputPrint, this);
			break;
		case "3":
			System.out.println("Enter type of task to delete");
			String input3 = inputScanner.getInput();
			System.out.println("Enter name of task to delete");
			String input4 = inputScanner.getInput();
			removeLineFromFile.removeLineFromFile("Tasks1.txt", input4, input3);
			break;
		case "4":
			System.out.println(printText2.getText("Edit Tasks"));
			System.out.print(printText2.getText("menu.input"));
			String input5 = inputScanner.getInput();
			editTask.editTask("Tasks1.txt", input5);
			break;
		case "0":
			System.out.println("Quiting program.");
			break;
		default:
			System.out.print(printText.getText("menu.invalid"));
			break;
		}
	}

}