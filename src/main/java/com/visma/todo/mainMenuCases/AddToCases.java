package com.visma.todo.mainMenuCases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.repositoryWrite.InsertObjectToDB;

@Component
public class AddToCases {

	@Autowired
	private InsertObjectToDB insert;

	public void handleAddToDBInput(String input, MainMenuCases mainMenuCases) {

		switch (input) {
		case "1":
			String reminder = "Reminder";
			insert.insertIntoDB(reminder,null);
			break;
		case "2":
			String meeting = "Meeting";
			insert.insertIntoDB(meeting,null);
			break;
		case "3":
			String ReserveTime = "ReserveTime";
			insert.insertIntoDB(ReserveTime,null);
			break;
		case "0":

			break;
		default:
			System.out.println("Sorry, but " + input + " is not one of " + "the menu choices. Please try again.");
			break;
		}
	}

}