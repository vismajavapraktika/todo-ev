package com.visma.todo.mainMenuCases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.repositoryRead.PrintInfoFromDB;

@Component
public class PrintFromDBCases {
	@Autowired
	private PrintInfoFromDB printInfoFromDB;


	public void handlePrintInput(String input, MainMenuCases mainMenuCases) {

		switch (input) {
		case "1":
			String infoReminder = printInfoFromDB.getRemindersListInfo();
			System.out.println(infoReminder);
			break;
		case "2":
			String infoMeeting = printInfoFromDB.getMeetingsListInfo();
			System.out.println(infoMeeting);
			break;
		case "3":
			String infoReserveTime = printInfoFromDB.getReserveTimesListInfo();
			System.out.println(infoReserveTime);
			break;
		case "4":
			String infoWholeList = printInfoFromDB.getWholeListInfo().toString();
			System.out.println(infoWholeList);
			break;
		case "0":
			break;
		default:
			System.out.println("Sorry, but " + input + " is not one of " + "the menu choices. Returning to main menu.");
			break;
		}
	}
}
