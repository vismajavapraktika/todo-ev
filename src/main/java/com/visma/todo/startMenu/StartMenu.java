package com.visma.todo.startMenu;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.visma.todo.printPlainText.TextMenuSelection;
import com.visma.todo.input.UserInput;
import com.visma.todo.mainMenuCases.MainMenuCases;

@Component
public class StartMenu{
	
	@Autowired
	private UserInput inputScanner;
	@Autowired
	private TextMenuSelection printText;
	@Autowired
	private MainMenuCases inputHandler;
	
//	@Override
//	public void run(String... args) throws IOException {
//		runMainMenu();
//	}


	public void runMainMenu() throws IOException{
		String input = null;
		do {
			System.out.println(printText.getText("Main menu"));
			System.out.print(printText.getText("menu.input"));
			input = inputScanner.getInput();
			inputHandler.handleMainMenuInput(input, this);
		} while (!input.equals("0"));
	}
}
