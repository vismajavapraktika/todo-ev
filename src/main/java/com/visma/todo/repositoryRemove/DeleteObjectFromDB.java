package com.visma.todo.repositoryRemove;
import com.visma.todo.objectBodies.Meeting;
import com.visma.todo.repositoryRead.PrintInfoFromDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;

import static com.visma.todo.DBConstants.DBConstants.*;

/**
 * Created by edmundas.virsila on 8/5/2016.
 */
@Component
public class DeleteObjectFromDB {
    @Autowired
    private PrintInfoFromDB collectedData;
    @Autowired
    private List<Meeting> wholeList;


    private static final String DELETE_ITEM = "DELETE FROM \"To_Do_Task\" WHERE \"Type\" = ? AND \"Name\" = ?";

    public void deleteObjectFromDB(String type, String name){
        wholeList = collectedData.getWholeListInfo();
        PreparedStatement statement = null;
        try (Connection conn = getConnection()) {
            statement = conn.prepareStatement(DELETE_ITEM);
            for (Meeting object : wholeList) {
                if (object.getType().equals(type) && object.getName().equals(name)) {
                    statement.setString(1, object.getType());
                    statement.setString(2, object.getName());
                    statement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(statement);
        }
    }

    public DeleteObjectFromDB() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    private static Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
