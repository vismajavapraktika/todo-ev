package com.visma.todo.repositoryRead;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.visma.todo.objectBodies.Meeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.CollectData.CollectDataFromSQL;

@Component
public class PrintInfoFromDB {

	final private static Map<String, String> textMap = printInfoFromDB();
	@Autowired
	private CollectDataFromSQL collectDataFromSQL;
	
	private static Map<String, String> printInfoFromDB() {
		Map<String, String> captions = new HashMap<>();
		captions.put("Info From DB",
				"======================================\n" + "What you want to print?\n" + "1: Reminder List.\n"
						+ "2: Meeting List.\n" + "3: Reserve time List.\n" + "4: Print all Tasks\n" + "0: Quit\n "
						+ "======================================");
		return captions;
	}

	public String getText(String key) {
		return textMap.get(key);
	}

	public String getRemindersListInfo() {
		return collectDataFromSQL.getReminderList().toString();
	}

	public List<Meeting> getWholeListInfo() {	return collectDataFromSQL.getWholeList();
	}

	public String getMeetingsListInfo() {
		return collectDataFromSQL.getMeetingList().toString();
	}

	public String getReserveTimesListInfo() {
		return collectDataFromSQL.getReserveTimeList().toString();
	}
}