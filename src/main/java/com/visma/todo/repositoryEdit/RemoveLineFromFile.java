package com.visma.todo.repositoryEdit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class RemoveLineFromFile {

	public static void removeLineFromFile(String file, String name) throws IOException {

		File inputFile = new File(file);
		File tempFile = new File("temp.txt");

		String sCurrentLine;
		BufferedReader fileRead = new BufferedReader(new FileReader(file));
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(tempFile));

		while ((sCurrentLine = fileRead.readLine()) != null) {
			String[] token = sCurrentLine.split(";");

			if (token[0].equals("Meeting")) {
				if (token.length > 6) {
					if (token[1].equals(name)) {
						continue;
					} else {
						for (int i = 0; i < 7; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
			} else {
				if (token.length > 5) {
					if (token[1].equals(name)) {
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
			}

			fileWriter.append("\r\n");
		}
		fileWriter.close();
		fileRead.close();
		inputFile.delete();

		boolean successful = tempFile.renameTo(inputFile);
		System.out.println(successful);

	}
}
