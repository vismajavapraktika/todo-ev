package com.visma.todo.repositoryEdit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.visma.todo.input.UserInput;
import com.visma.todo.input.UserKeyboardInput;

public class ChangeParameter {

	private final static String fileName = "tasks1.txt";
	private static UserInput inputScanner = new UserKeyboardInput();

	public static void changeParameter(String type, String name, String input) throws IOException {

		File inputFile = new File(fileName);
		File tempFile = new File("temp.txt");
		String sCurrentLine;
		BufferedReader fileRead = new BufferedReader(new FileReader(fileName));
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(tempFile));
		while ((sCurrentLine = fileRead.readLine()) != null) {
			String[] token = sCurrentLine.split(";");

			switch (input) {
			case "1":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[0] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "2":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[1] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "3":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[2] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "4":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[3] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "5":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[4] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "6":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[5] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			case "7":
				if (token.length > 5) {
					if (token[1].equals(name)) {
						token[6] = inputScanner.getInput();
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
						continue;
					} else {
						for (int i = 0; i < 6; i++) {
							fileWriter.append(token[i]);
							fileWriter.append(";");
						}
					}
				}
				break;
			default:
				break;
			}
			fileWriter.append("\r\n");
		}
		fileWriter.close();
		fileRead.close();
		inputFile.delete();

		boolean successful = tempFile.renameTo(inputFile);
		System.out.println(successful);

	}
}
