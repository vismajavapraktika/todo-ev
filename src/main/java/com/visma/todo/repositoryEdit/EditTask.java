package com.visma.todo.repositoryEdit;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.input.UserInput;
import com.visma.todo.printPlainText.TextChangeTaskData;


@Component
public class EditTask {

	@Autowired
	private UserInput inputScanner;
	@Autowired
	private TextChangeTaskData printText;

	public void editTask(String fileName, String input) throws IOException {

		switch (input) {
		case "1":
			System.out.println("Enter name of Reminder :");
			String input1 = inputScanner.getInput();
			System.out.println(printText.getText("1"));
			String input2 = inputScanner.getInput();
			ChangeParameter.changeParameter("Reminder", input1, input2);
			break;
		case "2":
			System.out.println("Enter name of Meeting :");
			String input3 = inputScanner.getInput();
			System.out.println(printText.getText("2"));
			String input4 = inputScanner.getInput();
			ChangeParameter.changeParameter("Meeting", input3, input4);
			break;
		case "3":
			System.out.println("Enter name of ReserveTime :");
			String input5 = inputScanner.getInput();
			System.out.println(printText.getText("3"));
			String input6 = inputScanner.getInput();
			ChangeParameter.changeParameter("ReserveTime", input5, input6);
			break;
		default:
			System.out.println("Sorry, but " + input + " is not one of " + "the menu choices. Please try again.");
			break;
		}
	}
}
