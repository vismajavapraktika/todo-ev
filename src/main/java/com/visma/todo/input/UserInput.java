package com.visma.todo.input;

public interface UserInput {
	String getInput();
}
