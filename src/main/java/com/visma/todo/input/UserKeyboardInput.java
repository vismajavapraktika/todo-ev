package com.visma.todo.input;

import java.util.Scanner;

import org.springframework.stereotype.Component;

@Component
public class UserKeyboardInput implements UserInput {

	@SuppressWarnings("resource")
	public String getInput() {
		final Scanner keyboard = new Scanner(System.in);
		return keyboard.nextLine();
	}
}
