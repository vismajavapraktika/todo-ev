package com.visma.todo.toolsForDateType;

import java.time.Duration;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.CollectData.CollectDataFromSQL;
import com.visma.todo.objectBodies.Meeting;
@Component
public class CheckIfTimeDoesntCollideWithOtherTime {

	@Autowired
	private CollectDataFromSQL collectDataFromSQL;
	@Autowired
	private ParseDate parse;


	public boolean checkingEnteredTimWithOtherTimes(String type, Date date, Duration duration) {



		for (Meeting wholeList : collectDataFromSQL.getWholeList()) {
			if (type.equals("Reminder")) {
				if ("ReserveTime".equals(wholeList.getType()) && !TwoDatesCollisionCheck.twoDatesCollisionCheck(date,
						duration, parse.convertStringToDate(wholeList.getDate()), wholeList.getDuration())) {
					return false;
				} else {
					return true;
				}
			} else if (type.equals("Meeting")) {
				if ("ReserveTime".equals(wholeList.getType())
						&& !TwoDatesCollisionCheck.twoDatesCollisionCheck(date, duration, parse.convertStringToDate(wholeList.getDate()),
								wholeList.getDuration())
						|| "Meeting".equals(wholeList.getType()) && !TwoDatesCollisionCheck.twoDatesCollisionCheck(date,
								duration, parse.convertStringToDate(wholeList.getDate()), wholeList.getDuration())) {
					return false;
				} else {
					return true;
				}
			} else {
				if (!TwoDatesCollisionCheck.twoDatesCollisionCheck(date, duration, parse.convertStringToDate(wholeList.getDate()),
						wholeList.getDuration())) {
					return false;
				} else {
					return true;
				}

			}

		}
		return false;
	}
}
