package com.visma.todo.toolsForDateType;

import java.util.Date;

public class CheckIfDateIsntOld {

	public static boolean checkIfNotOld(Date date) {

		Date nowDate = new Date();

		if (date.after(nowDate)) {
			return true;
		} else {
			return false;
		}

	}
}
