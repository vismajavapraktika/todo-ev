package com.visma.todo.toolsForDateType;

import java.time.Duration;
import java.util.Date;

public class AddDurationToDate {

	public static Date addDurationToDate(Date date, Duration duration) {

		long dateInSeconds = date.getTime()/1000;		
		date.setTime((dateInSeconds + duration.getSeconds())*1000);
		return date;

	}
}
