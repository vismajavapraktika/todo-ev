package com.visma.todo.toolsForDateType;

import java.time.Duration;
import java.util.Date;

public class TwoDatesCollisionCheck {

	public static boolean twoDatesCollisionCheck(Date date1, Duration duration1, Date date2, Duration duration2) {

		boolean after = date1.after(AddDurationToDate.addDurationToDate(date2, duration2));
		boolean before = AddDurationToDate.addDurationToDate(date1, duration1).before(date2);
		if (!(after || before)) {
			return true;
		} else {
			return false;
		}
	}

}
