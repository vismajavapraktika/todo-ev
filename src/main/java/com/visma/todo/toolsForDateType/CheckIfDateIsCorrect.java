package com.visma.todo.toolsForDateType;

import java.util.Calendar;
import java.util.Date;

public class CheckIfDateIsCorrect {
	private static int year;
	private static int month;
	private static int day;
	private static int hours;
	private static int minutes;
	private static int seconds;

	public static boolean checkIfDateIsCorrect(Date date) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH); 
		day = c.get(Calendar.DAY_OF_MONTH);
		hours = c.get(Calendar.HOUR);
		minutes = c.get(Calendar.MINUTE);
		seconds = c.get(Calendar.SECOND);

		
		
		if ( year >= 0 && month >= 0 && month <= 11 && day > 0
				&& day <= c.getActualMaximum(Calendar.DAY_OF_MONTH) && hours >= 0
				&& hours < 24 && minutes >= 0 && minutes < 60 && seconds >= 0
				&& seconds < 60) {
			return true;
		} else {
			return false;
		}

	}
}
