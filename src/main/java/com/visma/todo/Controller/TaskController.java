package com.visma.todo.Controller;

import com.visma.todo.CollectData.CollectDataForSQL;
import com.visma.todo.objectBodies.Meeting;
import com.visma.todo.objectBodies.MeetingDeleteReq;
import com.visma.todo.repositoryRead.PrintInfoFromDB;
import com.visma.todo.repositoryRemove.DeleteObjectFromDB;
import com.visma.todo.repositoryWrite.InsertObjectToDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.validation.Valid;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private PrintInfoFromDB wholeList;
    @Autowired
    private InsertObjectToDB objectInsertion;
    @Autowired
    private DeleteObjectFromDB deleteObject;

    @ModelAttribute("meeting")
    public Meeting getMeetingObject() {
        return new Meeting();
    }


    @ModelAttribute("wholeList")
    public List<Meeting> wholeList() {
        return wholeList.getWholeListInfo();
    }

    @RequestMapping("/to-do-tasks/view")
    public String viewAllTaskPage() {
        return "toDoTasks/view";
    }

    @RequestMapping("/to-do-tasks/new")
    public String saveNewTask() {
        System.out.println("entered new");
        return "/toDoTasks/new";
    }

    @RequestMapping(value = "/to-do-tasks/new", method = RequestMethod.POST)
    public String saveNewPost(Meeting meeting, BindingResult result) {
//        if(result.hasErrors()){
//            return "toDoTasks/new";}
        System.out.println("entered new meeting stuff");
        objectInsertion.insertIntoDB(meeting.getType(), meeting);
        return "redirect:/";
    }

    @RequestMapping(value = "/to-do-tasks/delete", method = RequestMethod.POST)
    public String deleteOldPost(MeetingDeleteReq deleteReq) {
        String name = deleteReq.getName();
        String type = deleteReq.getType();
        deleteObject.deleteObjectFromDB(type, name);
        return "redirect:/to-do-tasks/view";
    }


//    @RequestMapping("/toDoTasks/create/{id}")
//    public String editTask(@PathVariable id, Model model) {
//        //toDo load Task by id
//        model.addAttribute("task", loadedTask);
//        return "toDoTasks/create";
//    }
}