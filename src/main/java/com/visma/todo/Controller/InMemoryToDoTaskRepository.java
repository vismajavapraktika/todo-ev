package com.visma.todo.Controller;


import org.springframework.stereotype.Repository;

import com.visma.todo.objectBodies.Meeting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class InMemoryToDoTaskRepository implements ToDoTaskRepository {
    private Map<Integer, Meeting> ToDoTasks = new HashMap<>();

    @Override
    public List<Meeting> findAll() {
        return ToDoTasks.values()
                .stream()
                .collect(Collectors.toList());
    }

	@Override
	public Meeting save(Meeting meeting) {
		ToDoTasks.put(meeting.getId(), meeting);
		return meeting;
	}

	@Override
	public Meeting get(Integer id) {
		return ToDoTasks.get(id);
	}
}