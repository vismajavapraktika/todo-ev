package com.visma.todo.Controller;

import java.util.List;

import com.visma.todo.objectBodies.Meeting;

public interface ToDoTaskRepository{

	    List<Meeting> findAll();

	    Meeting save(Meeting meeting);

		Meeting get(Integer id);
	}
