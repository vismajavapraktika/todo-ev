package com.visma.todo.printPlainText;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class TextMenuSelection implements PrintText {

	final private static Map<String, String> textMap = getDefaultCaptions();

	private static Map<String, String> getDefaultCaptions() {
		Map<String, String> captions = new HashMap<>();
		captions.put("Main menu",
				"======================================\n" + "Enter number for menu choice:\n" + "1. Add a task\n"
						+ "2. List all tasks\n" + "3. Delete a task\n" + "4: Edit Task\n" + "0 :If you want to quit\n"
						+ "======================================");
		captions.put("menu.input", "Your input: ");
		captions.put("menu.invalid", "Invalid command");
		return captions;
	}

	@Override
	public String getText(String key) {
		return textMap.get(key);
	}
}
