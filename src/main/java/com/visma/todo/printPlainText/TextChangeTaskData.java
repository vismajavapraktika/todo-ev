package com.visma.todo.printPlainText;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class TextChangeTaskData implements PrintText {

	final private static Map<String, String> textMap = getDefaultCaptions();

	private static Map<String, String> getDefaultCaptions() {
		Map<String, String> captions = new HashMap<>();
		captions.put("Edit Tasks",
				"======================================\n" + "Enter number for menu choice:\n" + "1. Edit Reminder\n"
						+ "2. Edit Meeting\n" + "3. Edit Reserve Time\n" + "If you want to quit, enter anything else\n"
						+ "======================================");
		captions.put("1",
				"======================================\n" + "Enter number for menu choice:\n" + "1. Edit type\n"
						+ "2. Edit name\n" + "3. Edit action \n" + "4. Edit date\n" + "5. Edit duration\n"
						+ "6. Edit place\n" + "======================================");
		captions.put("2",
				"======================================\n" + "Enter number for menu choice:\n" + "1. Edit type\n"
						+ "2. Edit name\n" + "3. Edit action \n" + "4. Edit date\n" + "5. Edit duration\n"
						+ "6. Edit place\n" + "7. edit attendess" + "======================================");
		captions.put("3",
				"======================================\n" + "Enter number for menu choice:\n" + "1. Edit type\n"
						+ "2. Edit name\n" + "3. Edit action \n" + "4. Edit date\n" + "5. Edit duration\n"
						+ "6. Edit place\n" + "======================================");
		captions.put("menu.input", "Your input: ");
		captions.put("menu.invalid", "Invalid command");
		return captions;
	}

	@Override
	public String getText(String key) {
		return textMap.get(key);
	}
}
