package com.visma.todo.printPlainText;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class TextWritingToFile implements PrintText {
	final private static Map<String, String> textMap = getDefaultCaptions();

	private static Map<String, String> getDefaultCaptions() {
		Map<String, String> captions = new HashMap<>();
		captions.put("Writing to file menu choices",
				"======================================\n" + "What type task u want to create?\n" + "1: Reminder.\n"
						+ "2: Meeting.\n" + "3: Reserve time.\n" + "0: Quit\n "
						+ "======================================");
		captions.put("menu.input", "Your input: ");
		captions.put("menu.invalid", "Invalid command");
		return captions;
	}

	@Override
	public String getText(String key) {
		return textMap.get(key);
	}
}
