package com.visma.todo.repositoryWrite;

import static com.visma.todo.DBConstants.DBConstants.DB_DRIVER;
import static com.visma.todo.DBConstants.DBConstants.DB_PASSWORD;
import static com.visma.todo.DBConstants.DBConstants.DB_URL;
import static com.visma.todo.DBConstants.DBConstants.DB_USERNAME;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import com.visma.todo.objectBodies.Meeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.visma.todo.CollectData.CollectDataForSQL;
import com.visma.todo.objectCheck.CheckObjectValidation;
import com.visma.todo.toolsForDateType.ParseDate;

@Component
public class InsertObjectToDB {
	
	private static final String INSERT_TASK = "INSERT INTO \"To_Do_Task\" (\"ID\",\"Type\",\"Name\",\"Actions\",\"Date\",\"Place\",\"Attendess\",\"Duration\") VALUES (?,?,?,?,?,?,?,?) ";
	@Autowired
	private ParseDate parser;
	
	private static boolean check = false;

//	@Autowired
//	private CheckObjectValidation checkObjectValidation;
	
	public void insertIntoDB(String key, Meeting meeting) {

//		while (!check) {
//			CollectDataForSQL.createObject(key);
//			if (key.equals("Meeting")) {
//				check = checkObjectValidation.isMeetingCorrect(CollectDataForSQL.getMeeting());
//				if (!check) {
//					System.out.println("One of parameters are wrong");
//				}
//			} else if (key.equals("Reminder")) {
//				check = checkObjectValidation.isReminderCorrect(CollectDataForSQL.getReminder());
//				if (!check) {
//					System.out.println("One of parameters are wrong");
//				}
//			} else {
//				check = checkObjectValidation.isReserveTimeCorrect(CollectDataForSQL.getReserveTime());
//				if (!check) {
//					System.out.println("One of parameters are wrong");
//				}
//			}
//		}
//		check = false;

//		CollectDataForSQL.createObject(key);
		PreparedStatement statement = null;
		try (Connection conn = getConnection()) {
			statement = conn.prepareStatement(INSERT_TASK);
			if (key.equals("Reminder")) {
				statement.setInt(1, 1);
				statement.setString(2, meeting.getType());
				statement.setString(3, meeting.getName());
				statement.setString(4, meeting.getActions());
				statement.setTimestamp(5,
						Timestamp.valueOf(meeting.getDate()));
				statement.setString(6, meeting.getPlace());
				statement.setString(7, "");
				statement.setLong(8, meeting.getDuration().getSeconds());
				statement.executeUpdate();
			} else if (key.equals("Meeting")) {
				statement.setInt(1, 2);
				statement.setString(2, meeting.getType());
				statement.setString(3, meeting.getName());
				statement.setString(4, meeting.getActions());
				statement.setTimestamp(5,
						Timestamp.valueOf(meeting.getDate()));
				statement.setString(6, meeting.getPlace());
				statement.setString(7, meeting.getListOfAttendess());
				statement.setLong(8, meeting.getDuration().getSeconds());
				statement.executeUpdate();
			} else {
				statement.setInt(1, 3);
				statement.setString(2, meeting.getType());
				statement.setString(3, meeting.getName());
				statement.setString(4, meeting.getActions());
				statement.setTimestamp(5,
						Timestamp.valueOf(meeting.getDate()));
				statement.setString(6, meeting.getPlace());
				statement.setString(7, "");
				statement.setLong(8, meeting.getDuration().getSeconds());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeStatement(statement);
		}
	}
	
	public InsertObjectToDB() {
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	
	private static Connection getConnection() {
		try {
			return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private static void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

}
